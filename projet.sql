-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 09, 2017 at 11:27 PM
-- Server version: 5.7.17-0ubuntu0.16.04.1
-- PHP Version: 7.0.13-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `projet`
--

-- --------------------------------------------------------

--
-- Table structure for table `flots`
--

CREATE TABLE `flots` (
  `idflot` int(11) NOT NULL,
  `gameID` int(11) NOT NULL,
  `flotx` int(11) NOT NULL,
  `floty` int(11) NOT NULL,
  `idjoueursrc` int(11) NOT NULL,
  `idpdest` int(11) NOT NULL,
  `flotpop` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `flots`
--

INSERT INTO `flots` (`idflot`, `gameID`, `flotx`, `floty`, `idjoueursrc`, `idpdest`, `flotpop`) VALUES
(1, 27, 4, 5, 4, 14, 89),
(2, 27, 3, 4, 6, 12, 57);

-- --------------------------------------------------------

--
-- Table structure for table `game`
--

CREATE TABLE `game` (
  `id` int(11) NOT NULL,
  `joueurs` int(11) NOT NULL,
  `lignes` int(11) NOT NULL,
  `colonnes` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `game`
--

INSERT INTO `game` (`id`, `joueurs`, `lignes`, `colonnes`) VALUES
(26, 5, 5, 5),
(27, 5, 5, 5);

-- --------------------------------------------------------

--
-- Table structure for table `planetes`
--

CREATE TABLE `planetes` (
  `id` int(11) NOT NULL,
  `gameid` int(11) NOT NULL,
  `idplanete` int(11) NOT NULL,
  `x` int(11) NOT NULL,
  `y` int(11) NOT NULL,
  `idjoueur` int(11) DEFAULT NULL,
  `pcroissance` int(11) DEFAULT NULL,
  `population` int(11) NOT NULL,
  `icon` varchar(64) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `planetes`
--

INSERT INTO `planetes` (`id`, `gameid`, `idplanete`, `x`, `y`, `idjoueur`, `pcroissance`, `population`, `icon`) VALUES
(368, 26, 1, 3, 1, 1, 8, 345, 'img/EarthXP.ico'),
(369, 26, 2, 4, 2, 2, 5, 214, 'img/ErisXP.ico'),
(370, 26, 3, 4, 5, 3, 2, 331, 'img/JupiterXP.ico'),
(371, 26, 4, 5, 5, 4, 10, 120, 'img/MarsXP.ico'),
(372, 26, 5, 5, 2, 5, 2, 376, 'img/MercuryXP.ico'),
(373, 26, 6, 2, 4, NULL, 0, 114, 'img/NeptuneXP.ico'),
(374, 26, 7, 4, 3, NULL, 0, 350, 'img/PlutoXP.ico'),
(375, 26, 8, 5, 4, NULL, 0, 319, 'img/SaturnXP.ico'),
(376, 26, 9, 1, 4, NULL, 0, 297, 'img/UranusXP.ico'),
(377, 26, 10, 1, 2, NULL, 0, 347, 'img/VenusXP.ico'),
(378, 26, 11, 3, 2, NULL, 0, 202, 'img/EarthXP.ico'),
(379, 26, 12, 4, 4, NULL, 0, 195, 'img/ErisXP.ico'),
(380, 26, 13, 1, 5, NULL, 0, 364, 'img/JupiterXP.ico'),
(381, 26, 14, 2, 5, NULL, 0, 124, 'img/MarsXP.ico'),
(382, 26, 15, 3, 4, NULL, 0, 267, 'img/MercuryXP.ico'),
(383, 27, 1, 3, 1, 1, 2, 50, 'img/EarthXP.ico'),
(384, 27, 2, 4, 2, 1, 9, 485, 'img/ErisXP.ico'),
(385, 27, 3, 4, 5, 2, 10, 83, 'img/JupiterXP.ico'),
(386, 27, 4, 5, 5, 2, 6, 64, 'img/MarsXP.ico'),
(387, 27, 5, 5, 2, 1, 4, 223, 'img/MercuryXP.ico'),
(388, 27, 6, 2, 4, 2, 7, 152, 'img/NeptuneXP.ico'),
(389, 27, 7, 4, 3, NULL, 0, 350, 'img/PlutoXP.ico'),
(390, 27, 8, 5, 4, NULL, 0, 319, 'img/SaturnXP.ico'),
(391, 27, 9, 1, 4, NULL, 0, 297, 'img/UranusXP.ico'),
(392, 27, 10, 1, 2, NULL, 0, 347, 'img/VenusXP.ico'),
(393, 27, 11, 3, 2, NULL, 0, 202, 'img/EarthXP.ico'),
(394, 27, 12, 4, 4, 2, 6, 7, 'img/ErisXP.ico'),
(395, 27, 13, 1, 5, NULL, 0, 364, 'img/JupiterXP.ico'),
(396, 27, 14, 2, 5, 2, 5, 83, 'img/MarsXP.ico'),
(397, 27, 15, 3, 4, 2, 5, 62, 'img/MercuryXP.ico');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `flots`
--
ALTER TABLE `flots`
  ADD PRIMARY KEY (`idflot`),
  ADD KEY `gameID` (`gameID`);

--
-- Indexes for table `game`
--
ALTER TABLE `game`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `planetes`
--
ALTER TABLE `planetes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `gameid` (`gameid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `flots`
--
ALTER TABLE `flots`
  MODIFY `idflot` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `game`
--
ALTER TABLE `game`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `planetes`
--
ALTER TABLE `planetes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=398;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `planetes`
--
ALTER TABLE `planetes`
  ADD CONSTRAINT `gameID` FOREIGN KEY (`gameid`) REFERENCES `game` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
