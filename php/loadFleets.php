<?php
	require "connect.php";

	header("Content-type: application/json");

	$id = $_GET['gid'];
	
	$query = "SELECT * FROM flots WHERE gameID = " . $id;
	
	$res = $connexion -> query($query);	
		
	$i = 0;

	while ($rep = $res -> fetch()) {
		// JS: Planet(id, playerid, x, y, population, growth)
		$tmp = array(			
			"x" => $rep['flotx'],
			"y" => $rep['floty'],
			"source" => $rep['idjoueursrc'],
			"destination" => $rep ['idpdest'],
			"population" => $rep['flotpop']
		);

		$fleets[$i] = $tmp;
		$i++;
	}

	echo json_encode($fleets);
?>
