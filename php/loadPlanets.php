<?php
	require "connect.php";

	header("Content-type: application/json");

	$id = $_GET['gid'];
	
	$query = "SELECT * FROM planetes WHERE gameid = " . $id;
	
	$res = $connexion -> query($query);	
		
	$i = 0;

	while ($rep = $res -> fetch()) {
		// JS: Planet(id, playerid, x, y, population, growth)
		$tmp = array(
			"id" => $rep['idplanete'],
			"playerid" => $rep['idjoueur'],
			"x" => $rep['x'],
			"y" => $rep['y'],
			"population" => $rep['population'],
			"growth" => $rep ['pcroissance'],
			"icon" => $rep['icon']
		);

		$planets[$i] = $tmp;
		$i++;
	}

	echo json_encode($planets);
?>
