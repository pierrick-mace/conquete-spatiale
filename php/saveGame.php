<?php
	require "connect.php";

	$game = json_decode($_POST['game'], true);
	$planets = json_decode($_POST['planets'], true);
	$fleets = json_decode($_POST['fleets'], true);

	$query = "INSERT INTO game(`joueurs`, `lignes`, `colonnes`) VALUES(" . $game['players'] . ", " . $game['lines'] . ", " . $game['columns'] . ")";
	
	$res = $connexion -> exec($query);

	$id = $connexion -> lastInsertId();

	for ($i = 0; $i < count($planets); $i++) {

		if (isset($planets[$i]['playerid'])) {
			$query = "INSERT INTO planetes(`gameid`, `idplanete`, `x`, `y`, `idjoueur`, `pcroissance`, `population`, `icon`) VALUES(" . $id . ", " . $planets[$i]['id'] . ", " . $planets[$i]['x'] . ", " . $planets[$i]['y'] . ", " . $planets[$i]['playerid'] . ", " . $planets[$i]['growth'] . ", " . $planets[$i]['population'] . ", \"" . $planets[$i]['icon'] . "\")";
		} else {
			$query = "INSERT INTO planetes(`gameid`, `idplanete`, `x`, `y`, `idjoueur`, `pcroissance`, `population`, `icon`) VALUES(" . $id . ", " . $planets[$i]['id'] . ", " . $planets[$i]['x'] . ", " . $planets[$i]['y'] . ", NULL, " . $planets[$i]['growth'] . ", " . $planets[$i]['population'] . ", \"" . $planets[$i]['icon'] . "\")";
		}

		$res = $connexion -> exec($query);
	}

	for ($i = 0; $i < count($fleets); $i++) {
		$query = "INSERT INTO flots(`gameID`, `flotx`, `floty`, `idjoueursrc`, `idpdest`, `flotpop`) VALUES(" . $id . ", " . $fleets[$i]['position']['x'] . ", " . $fleets[$i]['position']['y'] . ", " . $fleets[$i]['source']['id'] . ", " . $fleets[$i]['destination']['id'] . ", " . $fleets[$i]['size'] . ")";
		$connexion -> exec($query);
	}

?>