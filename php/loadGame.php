<?php
	require "connect.php";

	header("Content-type: application/json");

	$id = $_GET['gid'];

	$query = "SELECT * FROM game WHERE id = {$id}";
	
	$res = $connexion -> query($query);	
		
	$i = 0;

	while ($rep = $res -> fetch()) {
		// JS: Planet(id, playerid, x, y, population, growth)
		$tmp = array(
			"players" => $rep['joueurs'],
			"lines" => $rep['lignes'],
			"columns" => $rep['colonnes'],
		);

		$game[$i] = $tmp;
		$i++;
	}

	echo json_encode($game);
?>
