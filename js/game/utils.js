function coordsNotTaken(coords) {
	for (var i = 0; i < planets.length; i++) {
		if (planets[i].getPosition().equals(coords)) {
			return false;
		}
	}

	return true;
}

function generateRandomCoords(rows, columns) {
	var x = Math.floor((Math.random() * rows) + 1);
	var y = Math.floor((Math.random() * columns) + 1);
	var coords = new Coord(x, y);

	if (coordsNotTaken(coords)) {
		return coords;
	}

	return generateRandomCoords(rows, columns);
}

function getPlanetByCoords(coords) {
	for (var i = 0; i < planets.length; i++) {
		if (planets[i].getPosition().equals(coords)) {
			return planets[i];
		}
	}
	return null;
}

function getPlanetById(id) {
	for (var i = 0; i < planets.length; i++) {
		if (planets[i].getId() == id) {
			return planets[i];
		}
	}

	return null;
}

function getFleetByCoords(coords) {
	for (var i = 0; i < fleets.length; i++) {
		if (fleets[i].getPosition().equals(coords)) {
			return fleets[i];
		}
	}

	return null;
}

function makeNeutral(planet) {
	if (planet == null) {
		return;
	}

	planet.setPlayer(null);
	planet.setGrowth(0);
}

function getPlayerPlanets(id) {
	var playerPlanets = new Array();

	for (var i = 0; i < planets.length; i++) {
		if (planets[i].getPlayer() == id) {
			playerPlanets.push(planets[i]);
		}
	}

	return playerPlanets;
}

function getPlayerFleets(id) {
	var playerFleets = new Array();

	for (var i = 0; i < fleets.length; i++) {
		if (fleets[i].getPlayerid() == id) {
			playerFleets.push(fleets[i]);
		}
	}

	return playerFleets;
}

function getPlayerById(id) {
	for (var i = 0; i < playerArray.length; i++) {
		if(playerArray[i].getPlayerId() == id) {
			return playerArray[i];
		}
	}

	return null;
}

function checkPlayers() {
	for (var i = 0; i < playerArray.length; i++) {
		if (playerArray[i].getPlanets().length == 0 && playerArray[i].getFleets().length == 0) {
			$("#battlelog").append("<p>Joueur " + playerArray[i].getPlayerId() + " éliminé.</p>");
			
			if (playerArray[i].getPlayerId() == PLAYER_ID) {
				gameState = STATE_LOST;
				alert("Vous avez perdu");
				$("#turn").prop("disabled", true);
				$("#save").prop("disabled", true);
				return;
			}

			playerArray.splice(i, 1);

			if (playerArray.length == 1) {
				gameState = STATE_WON;
				alert("Vous avez gagné la partie");
				$("#turn").prop("disabled", true);
				$("#save").prop("disabled", true);
			}
		}
	}
}