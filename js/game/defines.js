// CONSTANTES

const ATTACK_QUEUE_LENGTH = 10;
const SELF_ATTACK = 0;
const ENEMY_ATTACK = 1;
const PLAYER_ID = 1;
const MAX_GROWTH = 10;
const MAX_POP = 300;
const MIN_POP = 100;
const ICON_PATH = "img/";

const TYPE_PLAYER = 0;
const TYPE_AI = 1;

const STATE_PLAYING = 0;
const STATE_LOST = 1;
const STATE_WON = 2;

// VARIABLES GLOBALES

var gameLoaded = false;
var playerArray = new Array();
var planets = new Array();
var fleets = new Array();
var game = new Game(0, 0, 0);
var attacks = new Array();
var planetIcons = new Array('EarthXP.ico', 'ErisXP.ico', 'JupiterXP.ico',
							'MarsXP.ico', 'MercuryXP.ico', 'NeptuneXP.ico', 
							'PlutoXP.ico', 'SaturnXP.ico', 'UranusXP.ico',
							'VenusXP.ico');
var backgroundEnabled = false;
var gameState = STATE_PLAYING;