function Player(playerid, type, planets, fleets) {
	this.playerid = playerid;
	this.type = type;
	this.planets = planets;
	this.fleets = fleets;

	this.getPlayerId = function() {
		return this.playerid;
	}

	this.getType = function() {
		return this.type;
	}

	this.getPlanets = function() {
		return this.planets;
	}

	this.getFleets = function() {
		return this.fleets;
	}
}