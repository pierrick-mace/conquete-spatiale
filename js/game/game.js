function Game(players, lines, columns) {
	this.players = players;
	this.lines = lines;
	this.columns = columns;

	this.getPlayers = function() {
		return this.players;
	}

	this.getLines = function() {
		return this.lines;
	}

	this.getColumns = function() {
		return this.columns;
	}
	
	this.setPlayers = function(players) {
		this.players = players;
	}
}