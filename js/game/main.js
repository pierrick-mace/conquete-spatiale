window.onload = function() {

	var srcSet = false;
	var destSet = false;	

	console.log("Game: ");
	console.log(game);

	if (game != null) {
		displayPlanets();
		displayFleets();
	}

	$(document).on("mouseenter", "td", function() {
		if ($(this).attr('idplanete') != "") {
			//$(this).addClass("hover");
			//$(this).children(".text").show();
		}

		$(this).removeClass("click");
	});

	$(document).on("mouseleave", "td", function() {
		if ($(this).attr('idplanete') != "") {
			//$(this).removeClass("hover");
			//$(this).children(".text").hide();
		}
	});

	$(document).on("click", "td", function() {
		if ($(this).attr('idplanete') != ""){
			$(this).addClass("click");
			if (!srcSet) {
				srcId  = $(this).attr('idplanete');
				srcSet = true;
			} 
			else if (!destSet) {
				var destId = $(this).attr('idplanete');
				destSet = true;
				var val = prompt("Entrez le nombre de colons à envoyer");
				
				srcSet = false;
				destSet = false;

				$(".click").removeClass("click");

				var srcPlanet = getPlanetById(srcId);
				var destPlanet = getPlanetById(destId);
				if (srcPlanet == null || destPlanet == null || val <= 0 ) {
					return alert('Parametres incorrects');
				}
				if (srcPlanet.getPlayer() != PLAYER_ID) {
					return alert("Vous ne possédez pas cette planète");
				}
				if (val > srcPlanet.getPopulation()) {
					return alert("Vous n'avez pas autant de population sur cette planète");
				}
				var srcPlanet = getPlanetById(srcId);

				var fleet = new Fleet(srcPlanet.getPlayer(), val, srcPlanet, destPlanet);
				fleets.push(fleet);

				var player = getPlayerById(srcPlanet.getPlayer());
				player.getFleets().push(fleet);

				srcPlanet.setPopulation(srcPlanet.getPopulation() -val);
				displayFleets();
			}
		}
	});

	$(document).on("click", "#turn", function() {
		if (gameState == STATE_PLAYING) {
			$("#saved").hide();
			updatePlanets();
			updateFleets();
			updateAttacks();
			ordi_play();
			displayPlanets();
			displayFleets();
			checkPlayers();	
		}
	});

	$(document).on("click", "#save", function() {
		saveGame();
	});

	$(document).on("click", "#showColors", function() {
		if(backgroundEnabled) {
			$("#grille .player").removeClass("player_background");
			$("#grille .enemy").removeClass("enemy_background");
			$("#grille .neutral").removeClass("neutral_background");
			backgroundEnabled = false;
		} else {
			$("#grille .player").addClass("player_background");
			$("#grille .enemy").addClass("enemy_background");
			$("#grille .neutral").addClass("neutral_background");
			backgroundEnabled = true;
		}
	});
}

