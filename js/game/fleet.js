function Fleet(playerid, size, source, destination) {
	this.playerid = parseInt(playerid);
	this.size = parseInt(size);
	this.source = source;
	this.destination = destination;
	this.position = source.getPosition();

	this.getPlayerid = function() {
		return this.playerid;
	}

	this.getSize = function() {
		return this.size;
	}

	this.getSource = function() {
		return this.source;
	}

	this.getDestination = function() {
		return this.destination;
	}

	this.getPosition = function() {
		return this.position;
	}

	this.setPosition = function(newPosition) {
		this.position = newPosition;
	}

	this.atDestination = function() {
		return this.position.equals(this.destination.getPosition());
	}

	this.playTurn = function() {
		var destX = this.destination.getPosition().getX();
		var destY = this.destination.getPosition().getY();
		var currentX = this.getPosition().getX();
		var currentY = this.getPosition().getY();

		if (currentX != destX || currentY != destY) {
			if (destX > currentX) {
				currentX++;
			} else if (destX < currentX) {
				currentX--;
			} 

			if (destY > currentY) {
				currentY++;
			} else if (destY < currentY) {
				currentY--;
			}
	
			this.setPosition(new Coord(currentX, currentY));
		}
	}
}
