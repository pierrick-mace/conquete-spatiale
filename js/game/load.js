function loadGame(id) {

	// Envoie une requête sur la page loadGame.php et créé les objets en fonction du résultat
	$.getJSON('php/loadGame.php', {gid: id}, function(data) {
		if (data) {
			game = new Game(data[0].players, data[0].lines, data[0].columns);

            $.getJSON('php/loadPlanets.php', {gid: id}, function(data){  
                if (data) {
                    for (var i = 0; i < data.length; i++) {
                        planets.push(new Planet(data[i].id, data[i].playerid, data[i].x, data[i].y, 
                                                    parseInt(data[i].population), parseInt(data[i].growth), data[i].icon));
                    }

                    $.getJSON('php/loadFleets.php', {gid: id}, function(data) {        
                        if (data) {
                            for (var i = 0; i < data.length; i++) {
                                var source = getPlanetById(parseInt(data[i].source));
                                var destination = getPlanetById(parseInt(data[i].destination));

                                if (source == null || destination == null) {
                                    console.log('Error while finding planets');
                                }

                                var fleet = new Fleet(source.getPlayer(), parseInt(data[i].population), source, destination);
                                fleet.setPosition(new Coord(parseInt(data[i].x), parseInt(data[i].y)));
                                fleets.push(fleet);
                            }
                        } 

                        for (var i = 1; i <= game.getPlayers(); i++) {
                            var player;

                            var playerPlanets = getPlayerPlanets(i);
                            var playerFleets = getPlayerFleets(i);

                            if (playerPlanets.length > 0 || playerFleets.length > 0) {

                                if (i == PLAYER_ID) {
                                    player = new Player(i, TYPE_PLAYER, playerPlanets, playerFleets);
                                } else {
                                    player = new Player(i, TYPE_AI, playerPlanets, playerFleets);
                                }

                                playerArray.push(player);
                            }
                        }

                        gameState = STATE_PLAYING;
                        gameLoaded = true;

                        displayPlanets();
                        displayFleets();
                    });
                } else {
                    console.log('Error while loading planets');
                }
            });
		} else {
			console.log('Error while loading game');
		}
	});
}