function saveGame() {
	$.ajax({
	    type: 'POST',
	    url: 'php/saveGame.php',
	    data: {game: JSON.stringify(game), planets: JSON.stringify(planets), fleets: JSON.stringify(fleets)},
	    dataType: 'html'
	}).done( function( data ) {
	    $("#saved").html("<p>Partie sauvegardée avec succès</p>");
	    $("#saved").fadeIn();
	}).fail( function( data ) {
	    console.log('fail');
	    console.log(data);
	});
}