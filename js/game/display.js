function displayPlanets() {
	$("#grille").html("");

	for (var i = 1; i <= game.getLines(); i++) {
		$("#grille").append("<tr>");

		for (var j = 1; j <= game.getColumns(); j++) {
			var planet = getPlanetByCoords(new Coord(i, j));
		
			if (planet != null) {	

				if (planet.getPlayer() == PLAYER_ID) {
					if (backgroundEnabled) {
						$("#grille tr:last").append("<td idplanete = \"" + planet.getId() + "\" class = \"player player_background\">");
					} else {
						$("#grille tr:last").append("<td idplanete = \"" + planet.getId() + "\" class = \"player\">");
					}
				} else if (planet.getPlayer() == null) {
					if (backgroundEnabled) {
						$("#grille tr:last").append("<td idplanete = \"" + planet.getId() + "\" class = \"neutral neutral_background\">");
					} else {
						$("#grille tr:last").append("<td idplanete = \"" + planet.getId() + "\" class = \"neutral\">");
					}
				} else {
					if (backgroundEnabled) {
						$("#grille tr:last").append("<td idplanete = \"" + planet.getId() + "\" class = \"enemy enemy_background\">");
					} else {
						$("#grille tr:last").append("<td idplanete = \"" + planet.getId() + "\" class = \"enemy\">");
					}
				}

				$("#grille tr:last td:last").append("<div class = \"img\"> "
									+ "<img src = \"" + planet.getIcon() +"\" ></img>"
								    + "</div>");

				$("#grille tr:last td:last").append("<div class = \"popupcontainer\"><span class = \"text\">");

				if (planet.getPlayer() != null) {
					$("#grille tr:last td:last span:last").append("Planete " + planet.getId() 
						+ "<br/>Joueur " + planet.getPlayer()
						+ "<br/>Pop: " + planet.getPopulation() + "<br/>Croissance:"
						+ planet.getGrowth());
					//$("#grille tr:last td:last div:last").append("<p>Joueur " + planet.getPlayer() 
					//	+ "<br/>Planète " + planet.getId() + "</p>");
				} else {
					$("#grille tr:last td:last span:last").append("<p>Planète Neutre</p>");
				}

				$("#grille tr:last td:last").append("</span></div>");
				//$("#grille tr:last td:last").hide();
				$("#grille tr:last").append("</td>");
			} else {
				$("#grille tr:last").append("<td idplanete = \"-1\"></td>");
			}
		}

		$("#grille").append("</tr>");
	}

	//$("#planets").hide();
	$("#planets").html("<p>Planètes</p>");

	for (var i = 0; i < planets.length; i++) {
		if (planets[i].getPlayer() == PLAYER_ID) {
			$("#planets").append("<p>Planète " + planets[i].getId() + "<br/>Population " 
				+ planets[i].getPopulation() + "<br/>Croissance " + planets[i].getGrowth() + "</p>");
		}
	}

	$("#planets").fadeIn("fast");
}

function displayFleets() {
	displayPlanets();

	for (var i = 0; i < fleets.length; i++) {
		var x = fleets[i].getPosition().getX();
		var y = fleets[i].getPosition().getY();		

		$("#grille > tr:nth-child(" + x + ") > td:nth-child(" + y + ")").append(
					"<img src = \"img/spaceship.png\" height = \"35\" width = \"35\"></img>");
		

		if ($("#grille > tr:nth-child(" + x + ") > td:nth-child(" + y + ")").attr("idplanete") == "-1") {
			$("#grille > tr:nth-child(" + x + ") > td:nth-child(" + y + ")").append("<div class = \"popupcontainer\"><span class = \"text\">"
				+ "<span class = \"fleet\"><br/>---------------<br/>Flotte: <br/>Planete " + fleets[i].getSource().getId() + " --> Planete "
				+ fleets[i].getDestination().getId() + "<br/>Taille: " + fleets[i].getSize() + "</span>"
				+ "</span></div>");

			$("#grille > tr:nth-child(" + x + ") > td:nth-child(" + y + ")").attr("idplanete", "0");
		} else {
			$("#grille > tr:nth-child(" + x + ") > td:nth-child(" + y + ") > .popupcontainer").append(
			"<span class = \"fleet\"><br/>---------------<br/>Flotte: <br/>Planete " + fleets[i].getSource().getId() + " --> Planete "
					+ fleets[i].getDestination().getId() + "<br/>Taille: " + fleets[i].getSize() + "</span>");
		}
	}
}

function updatePlanets() {
	for (var i = 0; i < planets.length; i++) {
		if (planets[i].getPlayer() != null) {
			planets[i].playTurn();
		}
	}
}

function updateFleets() {
	for (var i = 0; i < fleets.length; i++) {
		fleets[i].playTurn();
	}
}

function updateAttacks() {
	for (var i = 0; i < fleets.length; i++) {
		if (fleets[i].atDestination()) {
			var source = fleets[i].getSource();
			var sourceId = fleets[i].getPlayerid();
			var destination = fleets[i].getDestination();
			var destinationId = fleets[i].getDestination().getPlayer();
			var attackersNb = fleets[i].getSize();
			var defendersNb = fleets[i].getDestination().getPopulation();

			if (attacks.length == ATTACK_QUEUE_LENGTH) {
				attacks.shift();	
			} 

			if (sourceId == destination.getPlayer()) {
					attacks.push(new Attack(source, sourceId, destination, destinationId, SELF_ATTACK, attackersNb, defendersNb));
			} else {
					attacks.push(new Attack(source, sourceId, destination, destinationId, ENEMY_ATTACK, attackersNb, defendersNb));
			}	

			attack(sourceId, destination, attackersNb);
	
			function findFleet(fl) {
				return fl == fleets[i];
			}

			var player = getPlayerById(sourceId);

			if (player != null) {
				var idx = player.getFleets().findIndex(findFleet);
				player.getFleets().splice(idx, 1);
			}

			fleets.splice(i, 1);
		}	
	}

	$("#battlelog").hide();
	$("#battlelog").html("<p>Batailles</p>");
	
	for (var i = (attacks.length - 1); i >= 0; i--) {

		$("#battlelog").append("<p>");
		if (attacks[i].getSourceId() == PLAYER_ID) {
			$("#battlelog").append("<span class = \"player\">Joueur " 
					+ attacks[i].getSourceId()
					+ "</span>");
		} else {
			$("#battlelog").append("<span class = \"enemy\">Joueur " 
					+ attacks[i].getSourceId()
					+ "</span>");
		}

		if (attacks[i].getType() == SELF_ATTACK) {
			$("#battlelog").append(" a envoyé " 
						+ attacks[i].getAttackers() + " troupes sur la ");
			
			if (attacks[i].getDestinationId() == PLAYER_ID) {
				$("#battlelog").append("<span class = \"player\">planète "
					+ attacks[i].getDestination().getId()
					+ "</span>");
			} else if (attacks[i].getDestinationId() != null) {
				$("#battlelog").append("<span class = \"enemy\">planète "
					+ attacks[i].getDestination().getId()
					+ "</span>");				
			} else {
				$("#battlelog").append("<span class = \"neutral\">planète "
					+ attacks[i].getDestination().getId()
					+ "</span>");
			}

		} else {
			if (attacks[i].getAttackers() > attacks[i].getDefenders()) {
				if (attacks[i].getDestinationId() == PLAYER_ID) {
					$("#battlelog").append(" a conquis la <span class = \"player\">planète "
						+ attacks[i].getDestination().getId()
						+ "</span>");
				} else if (attacks[i].getDestinationId() != null) {
					$("#battlelog").append(" a conquis la <span class = \"enemy\">planète "
						+ attacks[i].getDestination().getId()
						+ "</span>");				
				} else {
					$("#battlelog").append(" a conquis la <span class = \"neutral\">planète "
						+ attacks[i].getDestination().getId()
						+ "</span>");									
				}
				
				$("#battlelog").append(" avec "
								+ attacks[i].getAttackers() 
								+ " troupes");
			} else {
				if (attacks[i].getDestinationId() == PLAYER_ID) {
					$("#battlelog").append(" a échoué son attaque sur la <span class = \"player\">planète "
						+ attacks[i].getDestination().getId()
						+ "</span>");
				} else if (attacks[i].getDestinationId() != null) {
					$("#battlelog").append(" a échoué son attaque sur la <span class = \"enemy\">planète "
						+ attacks[i].getDestination().getId()
						+ "</span>");
				} else {
					$("#battlelog").append(" a échoué son attaque sur la <span class = \"neutral\">planète "
						+ attacks[i].getDestination().getId()
						+ "</span>");
				}
			}
		}
		$("#battlelog").append("</p>");
	}

	$("#battlelog").fadeIn("fast");
}