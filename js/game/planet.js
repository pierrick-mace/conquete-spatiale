function Planet(id, playerid, x, y, population, growth, icon) {
	this.id = id;
	this.playerid = playerid;
	this.x = x;
	this.y = y;
	this.population = population;
	this.growth = growth;
	this.icon = icon;

	this.getId = function() {
		return this.id;
	}

	this.getPlayer = function() {
		return this.playerid;
	}

	this.getPopulation = function() {
		return this.population;
	}

	this.getGrowth = function() {
		return this.growth;
	}

	this.getPosition = function() {
		return new Coord(parseInt(this.x), parseInt(this.y)); 
	}

	this.getIcon = function() {
		return this.icon;
	}

	this.setPlayer = function(playerid) {
		this.playerid = playerid;
	}

	this.setPopulation = function(population) {
		this.population = population;
	}

	this.setGrowth = function(growth) {
		this.growth = growth;
	}

	this.playTurn = function() {
		this.population += this.growth;
	}
}