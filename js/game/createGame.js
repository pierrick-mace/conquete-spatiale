function createGame(players, rows, columns, planetsNb) {

	game = new Game(players, rows, columns);

	for (var i = 1; i <= planetsNb; i++) {
		var icon =  (i-1)%planetIcons.length;
		var iconName = planetIcons[icon];
		var pop = Math.floor((Math.random() * MAX_POP) + MIN_POP);
		if (i > players) {
			var coords = generateRandomCoords(rows, columns);
			planets.push(new Planet(i, null, coords.getX(), coords.getY(), pop, 0, ICON_PATH + iconName));
		} else {
			var coords = generateRandomCoords(rows, columns);
			var growth = Math.floor((Math.random() * MAX_GROWTH) + 1);
			
			var planet = new Planet(i, i, coords.getX(), coords.getY(), pop, growth, ICON_PATH + iconName);

			var player;
			
			if (i == PLAYER_ID) {
				player = new Player(i, TYPE_PLAYER, new Array(planet), new Array());
			} else {
				player = new Player(i, TYPE_AI, new Array(planet), new Array());
			}

			playerArray.push(player);
			planets.push(planet);
		}
	}

	gameLoaded = true;
	gameState = STATE_PLAYING;

	/*console.log("loading planets");
	displayPlanets();
    displayFleets();*/
}