function Attack(source, sourceId, destination, destinationId, type, attackers, defenders) {
	this.source = source;
	this.sourceId = sourceId;
	this.destination = destination;
	this.destinationId = destinationId;
	this.type = type;
	this.attackers = attackers;
	this.defenders = defenders;

	this.getSource = function() {
		return this.source;
	}

	this.getSourceId = function() {
		return this.sourceId;
	}

	this.getDestination = function() {
		return this.destination;
	}

	this.getDestinationId = function() {
		return this.destinationId;
	}

	this.getType = function() {
		return this.type;
	}

	this.getAttackers = function() {
		return this.attackers;
	}

	this.getDefenders = function() {
		return this.defenders;
	}
}

function attack(srcId, destPlanet, settlers) {
	if (srcId == null || destPlanet == null) {
		return;
	}

	var destPlanetPopulation = destPlanet.getPopulation();

	if (srcId == destPlanet.getPlayer()) {
		destPlanet.setPopulation(destPlanetPopulation + settlers);
	} else {
		if (settlers > destPlanetPopulation) {
			if (destPlanet.getPlayer() != null) {
				function findPlanet(pl) {
					return pl == destPlanet;
				}

				var player = getPlayerById(destPlanet.getPlayer());

				if (player != null) {
					var idx = player.getPlanets().findIndex(findPlanet);
					player.getPlanets().splice(idx, 1);
				}
			}

			var player = getPlayerById(srcId);

			if (player != null) {
				player.getPlanets().push(destPlanet);
			}

			destPlanet.setPopulation(settlers - destPlanetPopulation);
			destPlanet.setPlayer(srcId);
			destPlanet.setGrowth(Math.floor((Math.random() * 10) + 1));
			
		} else if (settlers < destPlanetPopulation) {
			destPlanet.setPopulation(destPlanetPopulation - settlers);
		} else {
			if (destPlanet.getPlayer() != null) {
				function findPlanet(pl) {
					return pl == destPlanet;
				}

				var player = getPlayerById(destPlanet.getPlayer());
				if (player != null) {
					var idx = player.getPlanets().findIndex(findPlanet);
					player.getPlanets().splice(idx, 1);
				}
			}

			destPlanet.setPopulation(0);
			makeNeutral(destPlanet);
		}
	}
}