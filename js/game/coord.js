function Coord(x, y) {
	this.x = x;
	this.y = y;

	this.getX = function() {
		return this.x;
	}

	this.getY = function() {
		return this.y;
	}

	this.equals = function(object) {
		if ((object != null) && object instanceof Coord) {
			if(this.x == object.x && this.y == object.y) {
				return true;
			}
		}

		return false;
	}
}