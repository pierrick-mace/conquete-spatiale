<!DOCTYPE html>
<html>
	<head>
		<title>index</title>
		<meta charset="utf-8" />
		<link rel="stylesheet" href="css/index.css" />
		<script src="http://code.jquery.com/jquery-3.1.1.min.js"></script>

	</head>
	</body>
		<div class = "container">
			<div class = "child">
				<?php
					require "php/connect.php";

					$query = "SELECT * FROM game";
					$res = $connexion -> query($query);

					$result = $res -> fetchAll();

					if (count($result) > 0) {
						echo "<div class = \"gamesfound\">";

						if (count($result) == 1) {
							echo "<h2>Une partie est sauvegardée sur la base de données.</h2>";
						} else {
							echo "<h2>Plusieurs parties sont sauvegardées sur la base de données.</h2>";
						}

						echo "<form method = \"post\" action = \"game.php\">";
						echo "<select name = \"games\" id = \"selector\">";
						foreach ($result as $row) {
							echo "<option value = \"" . $row['id'] . "\">Partie: " . $row['id'] . " joueurs: " . $row['joueurs'] . "</option>";
						}

						echo "</select>";

						echo "<input type = \"submit\" name = \"continuer\" value = \"Continuer\" class = \"button\">
								</form>
							</div>";
					} 


				?>

				<h2>Créer nouvelle partie:</h2>

					<form method="post" action="game.php" nom="formulaire" onsubmit="return test()">
						   	<p><label>Nombre de joueurs:</label> <input class = "centered" id = "nbj" type="text" name="nbj" size ="5" oninput = "verif(this)" onkeypress = "verif(this)" onchange = "verif(this)" onpaste = "verif(this)"/> <br /></p>
							<p><label>Nombre de lignes:</label>   <input class = "centered" id = "nbl" type="text" name="nbl" size="5" oninput = "verif(this)" onkeypress = "verif(this)" onchange = "verif(this)" onpaste = "verif(this)" /> <br /></p>
							<p><label>Nombre de colones:</label> <input class = "centered" id = "nbc" type="text" name="nbc" size="5" oninput = "verif(this)" onkeypress = "verif(this)" onchange = "verif(this)" onpaste = "verif(this)"/> <br /></p>
					    	<input type = "submit" name = "new" value = "Nouvelle partie" class = "button" />
					</form>
			</div>
		</div>

			<script>
			function verif(champ) {
				if (isNaN(champ.value)) {
					champ.style.backgroundColor = "tomato";
				} else {
					champ.style.backgroundColor = "lime";
				}
			}

			function test() { // teste si le nom contient au moins 2 lettres
				var nbj = document.getElementById("nbj").value;
				var nbl = document.getElementById("nbl").value;
				var nbc = document.getElementById("nbc").value;

				return nbj != "" && nbl != "" && nbc != "" && !isNaN(nbj)&&!isNaN(nbl)&&!isNaN(nbc) ;
			}

			test();
		</script>
		
	</body>
</html>
