<!DOCTYPE html>
<html>
	<head>
		<title>Game</title>
		<meta charset = "utf-8" />
		<link rel="stylesheet" href= "css/game.css" />
		<script type="text/javascript" src="js/jquery-3.1.1.min.js"></script>
		<script type="text/javascript" src="js/game.min.js"></script>
	</head>

	<body>

		<?php
			if(isset($_POST['continuer'])) {
				// Charger partie en cours
				echo "<script>loadGame(" . $_POST['games'] . ");</script>";
			} else {
				// Créer nouvelle partie
				$joueurs = intval($_POST['nbj']);
				$colonnes = intval($_POST['nbc']);
				$lignes = intval($_POST['nbl']);

				$nbPlanetes = min($joueurs + 2  , $lignes * $colonnes);
				$nbPlanetes = min($joueurs + 10  , $lignes * $colonnes);

				echo "<script>createGame(" . $joueurs . "," . $lignes . "," . $colonnes . "," . $nbPlanetes . ")</script>";
			}
		?>

		<div class = "container">

			<div class = "sidebar">
				<button id = "turn">Tour suivant</button>
				<button id = "save">Sauvegarder Partie</button>
				<button id = "showColors">Planètes possédées</button>

				<div id = "saved"></div>

				<div class = "separator"></div>
				
				<div class="scrollbar">
  					<div class="force-overflow">
  						<div id = "planets"><p>Planètes</p></div>
  					</div>
				</div>
				
				<div class="scrollbar">
				  <div class="force-overflow">
				  	<div id = "battlelog"<p>Batailles</p></div>
				  </div>
				</div>
				


			</div>
	
			<div class = "main">
				<table id = "grille" class = "grille">
					
				</table>
			</div>
		</div>
	</body>
</html>
